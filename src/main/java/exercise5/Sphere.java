package exercise5;

public class Sphere extends Entity {

    Vec3D center;
    float radius;

    Sphere(Vec3D center, float radius) { 
        this.center = center; 
        this.radius = radius; 
    }

    @Override
    public boolean equals(Object o) {
        boolean result = false;
        if ((o instanceof Sphere) && (o != null)) {
            Sphere other = (Sphere) o;
            if (this.radius == other.radius) {
                result = true;
            }
        }
        return result;
    }

    public Vec3D intersect(Vec3D ray) {
        ray = ray.mul(1.0f / ray.len());
        float d1 = center.mul(ray);
        float cl = center.len();
        float d2 = d1*d1 - cl*cl + radius*radius;
        if (d2 < 0.0f) {
            return null;
        }
        float d = d1 - (float)Math.sqrt(d2);
        return ray.mul(d);
    }

    // https://www.gamedev.net/forums/topic/168338-sphere-surface-normal/
    
    @Override
    Vec3D getNormal(Vec3D point) {
        Vec3D positionVec = point.sub(center);
        return positionVec.norm();
    }
    
    int color() { return 0x00FF00; }

}
