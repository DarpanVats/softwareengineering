package exercise5;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Image {

	public byte[] data;
	int width, height;

	Image(int width, int height) {
		this.width = width;
		this.height = height;
		data = new byte[width * height * 3];
	}

	void set(int x, int y, int value) {
		int offset = (y * width + x) * 3;
		data[offset + 0] = (byte) ((value & 0x00FF0000) >> 16);
		data[offset + 1] = (byte) ((value & 0x0000FF00) >> 8);
		data[offset + 2] = (byte) ((value & 0x000000FF) >> 0);
	}

	void write(String filename) throws FileNotFoundException, IOException {
		FileOutputStream stream = new FileOutputStream(filename); // +".ppm");
		stream.write(new String("P6 " + width + " " + height + " 255\n").getBytes());
		stream.write(data);
		stream.close();
	}

	void trace(List<Entity> entities) {

		// iterate over all pixels
		for (int x = 0; x < width; x++)
			for (int y = 0; y < height; y++) {

				// convert the pixel coordiates into a 3D ray
				// if z component not approx. equal to x/y -> fisheye/zoom
				Vec3D ray = new Vec3D(x - (width / 2), y - (height / 2), (width + height) / 2);

				Vec3D LightRay = new Vec3D(1, -1/2, 0);
				Vec3D test = new Vec3D(0.82454705f, 0.5107505f, 0.2434261f);
				
				int col = 0x000000;
				int ShadeCol = 0x000000;
				float dist = Float.MAX_VALUE;

				for (Entity e : entities) {
					Vec3D pt = e.intersect(ray);
					if (pt == null)
						continue; // no intersection
					if (pt.z < 0.0)
						continue; // intersection behind camera
					if (dist < pt.len())
						continue; // intersection hidden by other object
					dist = pt.len();
					col = e.color();

					Vec3D normal = e.getNormal(pt);

					System.out.println("Color" + col);
			    
// dot product of normal to surface and light ray
			    
					float dotProduct = normal.mul(LightRay);
				
//					adjust color using ColorUtil
					ColorUtil colour = new ColorUtil();
					ShadeCol = colour.adjust(col, 0xC0C0C0 , dotProduct);
//					System.out.println(col);


				}
				set(x, height - 1 - y, ShadeCol);
			}
	}

}