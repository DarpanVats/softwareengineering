package exercise5;
import java.util.List;
import java.lang.reflect.Constructor;
import java.util.*;

class EntityFactory {

	protected Map<String, Class> factory;

	protected static EntityFactory instance = new EntityFactory();

	static EntityFactory get() {
		return instance;
	}

	protected EntityFactory() {
		factory = new LinkedHashMap<String, Class>();
		factory.put("Sphere", Sphere.class);
		factory.put("Plane", Plane.class);
		factory.put("Cone", Cone.class);
	}

	Entity create(String name) {

		// split into class name and parameters
		String[] tokens = name.split("\\{", 2);
		String type = tokens[0].trim();
		String params = tokens[1].trim();

		// split parameters into individual parts
		tokens = params.split(",");
		Object[] param_objs = new Object[tokens.length];

		// convert each parameter to Vec3D or Float object
		for (int i = 0; i < tokens.length; i++) {
			String token = tokens[i].trim();
			if (token.endsWith(")") | token.endsWith("}"))
				token = token.substring(0, token.length() - 1);
			if (token.startsWith("("))
				param_objs[i] = new Vec3D(token);
			else
				param_objs[i] = Float.valueOf(token);
		}

		// retrieve the first constructor
		Constructor<?> con = factory.get(type).getDeclaredConstructors()[0];
		Entity result;

		// call constructor with generic parameter list
		try {
			result = (Entity) con.newInstance(param_objs);
		} catch (Exception e) {
			result = null;
		}

		return result;
	}
}
