package exercise5;

class Color extends Entity {

	private Entity e;
	private int colr;

	// entity with an unique color
	Color( Entity e, int  color ) { 
		this.e = e; 
		this.colr= color; 
	}
	Vec3D intersect( Vec3D ray ) { 
		return e.intersect(ray); 
	}
	int color() { 
		return this.colr; 
	}
	Vec3D getNormal( Vec3D light ) {
		 Vec3D norm= this.e.getNormal(light);
		 return norm;
	}


}
