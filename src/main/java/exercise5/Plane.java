package exercise5;

public class Plane extends Entity{

    private Vec3D point,normal;

    Plane(Vec3D point, Vec3D normal) {
        this.point = point;
        this.normal = normal;
    }

    @Override
    public boolean equals(Object o) {
        boolean result = false;
        if ((o != null) && (o instanceof Plane)) {
            Plane otherPlane = (Plane) o;
            Vec3D n_this = this.normal.norm();
            Vec3D n_other = otherPlane.normal.norm();
            // Danger: Small deviations lead to False!
            if ((n_this.x == n_other.x) &&
                    (n_this.y == n_other.y) &&
                    (n_this.z == n_other.z)) {
                result = true;
                    }
        }
        return result;
    }

    @Override
    public Vec3D intersect(Vec3D ray) { 
        float d1 = point.mul(normal);
        float d2 = ray.mul(normal);
        if (d2 == 0.0) return null;
        return ray.mul(d1 / d2);
    }
    
    @Override
    Vec3D getNormal(Vec3D point) {
        /**
         * a plane is a set of points P where: (P - P0) * n = 0 means there is only one
         * n
         */
        return this.normal.norm();
    }
    
    int color() { return 0xFF0000; }

}
