package exercise5;

class ColorUtil {

	// split composite color value into individual R,G,B values
	static int[] split(int color) {
		int[] values = new int[3];
		values[0] = (color & 0x00FF0000) >> 16;
		values[1] = (color & 0x0000FF00) >> 8;
		values[2] = (color & 0x000000FF) >> 0;
		return values;
	}

	// make sure integer value is between 0 and 255
	static int clamp(int input) {
		return input > 0xFF ? 0xFF : (input < 0x00 ? 0x00 : input);
	}

	// adjust color according to dot product of normal
	static int adjust(int entity_color, int light_color, float dot_prod) {

		// extract individual color components
		int[] ec = split(entity_color);
		int[] lc = split(light_color);

		// create target color out of scaled light color and entity color
		int R = clamp(Math.round(dot_prod * lc[0]) + ec[0]);
		int G = clamp(Math.round(dot_prod * lc[1]) + ec[1]);
		int B = clamp(Math.round(dot_prod * lc[2]) + ec[2]);

		// merge into single int value and return
		return (R << 16) + (G << 8) + B;
	}

}
