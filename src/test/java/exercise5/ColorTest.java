package exercise5;

import org.junit.Test;

public class ColorTest {

	@Test
	public void ColorTest() throws java.io.FileNotFoundException, java.io.IOException {

		Group group = new Group();
		Image image = new Image(320, 240);

		group.add(new Color(new Plane(new Vec3D(0, -1, 0), new Vec3D(0, 1, 0)), 0x00FFFF00));
		group.add(new Color(new Sphere(new Vec3D(5, 0, 10), 5.0f), 0x00FF00FF));
		group.add(new Color(new Cone(new Vec3D(-5, 3, 10), new Vec3D(0.5f, -2, 0), 0.3f, 4.0f), 0x0000FFFF));

		image.trace(group.children);
		image.write("trace1.ppm");
	}

}
