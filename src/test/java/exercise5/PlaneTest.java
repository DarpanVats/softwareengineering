package exercise5;

import org.junit.Test;

import exercise5.Entity;
import exercise5.Plane;
import exercise5.Vec3D;

import static org.junit.Assert.*;

public class PlaneTest {

    final static double DELTA = 0.00001;

    // Plane tests
    @Test
    public void PlaneTest1() {
        Plane p = new Plane(new Vec3D(2, 0, 0), new Vec3D(1, 0, 0));
        Entity e = p;
        Vec3D r = e.intersect(new Vec3D(1, 1, 0));
        assertEquals(r.x, 2.0, DELTA);
        assertEquals(r.y, 2.0, DELTA);
        assertEquals(r.z, 0.0, DELTA);
    }

    @Test
    public void PlaneTest2() {
        Plane p = new Plane(new Vec3D(2, 0, 0), new Vec3D(1, 0, 0));
        Entity e = p;
        Vec3D r = e.intersect(new Vec3D(0, 1, 0));
        assertEquals(r, null);
    }

    @Test
    public void PlaneTestEquals1() {
        Plane p1 = new Plane(new Vec3D(2, 0, 0), new Vec3D(1, 0, 0));
        Plane p2 = new Plane(new Vec3D(2, 0, 0), new Vec3D(1, 0, 0));
        assertEquals(true, p1.equals(p2));
    }

    @Test
    public void PlaneTestEquals2() {
        Plane p1 = new Plane(new Vec3D(2, -4, 0), new Vec3D(-1, 0, 0));
        Plane p2 = new Plane(new Vec3D(2, 1, 3), new Vec3D(1, 0, 0));
        assertEquals(false, p1.equals(p2));
    }

    @Test
    public void PlaneTestEquals3() {
        Plane p1 = new Plane(new Vec3D(2, -4, 0), new Vec3D(1, 1, 1));
        Plane p2 = new Plane(new Vec3D(2, 1, 3), new Vec3D(3, 3, 3));
        assertEquals(true, p1.equals(p2));
    }

    @Test
    public void PlaneTestEquals4() {
        Plane p1 = new Plane(new Vec3D(0, 0, 0), new Vec3D(1, 0, 1) );
        Plane p2 = new Plane(new Vec3D(0, 0, 0), new Vec3D(0, 1, 1));
        assertEquals(false, p1.equals(p2));
    }

    @Test
    public void PlaneTestEquals5() {
        Plane p1 = new Plane(new Vec3D(0, 0, 0), new Vec3D(1, 0, 1));
        Plane p2 = null;
        assertEquals(false, p1.equals(p2));
    }
}
