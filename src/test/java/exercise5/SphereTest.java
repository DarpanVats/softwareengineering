package exercise5;

import org.junit.Test;

import exercise5.Entity;
import exercise5.Sphere;
import exercise5.Vec3D;

import static org.junit.Assert.*;

public class SphereTest {

    final static double DELTA = 0.00001;

    @Test
    public void SphereTest1() {
        Sphere s = new Sphere(new Vec3D(2, 0, 0), 1);
        Entity e = s;
        Vec3D r = e.intersect(new Vec3D(1, 0, 0));
        assertEquals(r.x, 1.0, DELTA);
        assertEquals(r.y, 0.0, DELTA);
        assertEquals(r.z, 0.0, DELTA);
    }

    @Test
    public void SphereTest2() {
        Sphere s = new Sphere(new Vec3D(2, 1, 0), 1);
        Entity e = s;
        Vec3D r = e.intersect(new Vec3D(1, 0, 0));
        assertEquals(r.x, 2.0, DELTA);
        assertEquals(r.y, 0.0, DELTA);
        assertEquals(r.z, 0.0, DELTA);
    }

    @Test
    public void SphereTest3() {
        Sphere s = new Sphere( new Vec3D(2, 2, 0), 1 );
        Entity e = s;
        Vec3D r = e.intersect( new Vec3D(1, 0, 0));
        assertEquals(r, null);
    }

    @Test
    public void SphereTestEquals1() {
        Sphere s1 = new Sphere(new Vec3D(2, 0, 0), 1 );
        Sphere s2 = new Sphere(new Vec3D(2, 0, 0), 1 );
        assertEquals(true, s1.equals(s2));
    }

    @Test
    public void SphereTestEquals2() {
        Sphere s1 = new Sphere(new Vec3D(-5, 2, 1), 3);
        Sphere s2 = new Sphere(new Vec3D(2, -1, 4), 3);
        assertEquals(true, s1.equals(s2));
    }

    @Test
    public void SphereTestEquals3() {
        Sphere s1 = new Sphere(new Vec3D(2, 0, 0), 1);
        Sphere s2 = new Sphere(new Vec3D(2, 0, 0), 0.5f);
        assertEquals(false, s1.equals(s2));
    }

    @Test
    public void SphereTestEquals4() {
        Sphere s1 = new Sphere(new Vec3D(1, 1, 1), 2);
        Sphere s2 = new Sphere(new Vec3D(0, 0, 0), 0.5f);
        assertEquals(false, s1.equals(s2));
    }

    @Test
    public void SphereTestEquals5() {
        Sphere s1 = new Sphere(new Vec3D(1, 1, 1), 2);
        Sphere s2 = null;
        assertEquals(false, s1.equals(s2));
    }
}
