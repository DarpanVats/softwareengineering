package exercise5;

import org.junit.Test;

import exercise5.Vec3D;

import static org.junit.Assert.*;
import static org.junit.Assert.fail;

public class Vec3DTest {

    // allows precision loss
    final static double DELTA = 0.01;

    @Test 
    public void Vec3DTestLen() {
        Vec3D v1 = new Vec3D(1, 0, 0);
        Vec3D v2 = new Vec3D(3, 0, 4);
        assertEquals(v1.len(), 1.0, DELTA);
        assertEquals(v2.len(), 5.0, DELTA);
    }

    @Test 
    public void Vec3DTestCross1() {
        Vec3D v1 = new Vec3D(1, 1, 0);
        Vec3D v2 = new Vec3D(0, 1, 0);
        Vec3D v3 = v1.cross(v2);
        assertEquals(v3.x, 0.0, DELTA);
        assertEquals(v3.y, 0.0, DELTA);
        assertEquals(v3.z, 1.0, DELTA);
    }

    @Test 
    public void Vec3DTestCross2() {
        Vec3D v1 = new Vec3D(1, 1, 0);
        Vec3D v2 = new Vec3D(0, 1, 0);
        Vec3D v3 = v2.cross(v1);
        assertEquals(v3.x,  0.0, DELTA);
        assertEquals(v3.y,  0.0, DELTA);
        assertEquals(v3.z, -1.0, DELTA);
    }

    @Test
    public void Vec3DTestNorm1() {
        Vec3D v1 = new Vec3D(1, 0, 0);
        assertEquals(1.0f, v1.norm().x, DELTA);
        assertEquals(0, v1.norm().y, DELTA);
        assertEquals(0, v1.norm().z, DELTA);
    }

    @Test
    public void Vec3DTestNorm2() {
        Vec3D v1 = new Vec3D(1, 1, 1);
        assertEquals(0.58f, v1.norm().x, DELTA);
        assertEquals(0.58f, v1.norm().y, DELTA);
        assertEquals(0.58f, v1.norm().z, DELTA);
    }

    @Test
    public void Vec3DTestNorm3() {
        Vec3D v1 = new Vec3D(3, 4, 5);
        assertEquals(0.42f, v1.norm().x, DELTA);
        assertEquals(0.57f, v1.norm().y, DELTA);
        assertEquals(0.71f, v1.norm().z, DELTA);
    }

    // Test cases for multiplication with scalar
    @Test
    public void Vec3DTestMul1() {
        Vec3D v1 = new Vec3D(1, 3, 2);
        float scalar = 2;
        Vec3D result = v1.mul(scalar);
        assertEquals(2, result.x, DELTA);
        assertEquals(6, result.y, DELTA);
        assertEquals(4, result.z, DELTA);
    }

    @Test
    public void Vec3DTestMul2() {
        Vec3D v1 = new Vec3D(1, 3, 2);
        float scalar = 0;
        Vec3D result = v1.mul(scalar);
        assertEquals(0, result.x, DELTA);
        assertEquals(0, result.y, DELTA);
        assertEquals(0, result.z, DELTA);
    }

    @Test (expected=NullPointerException.class)
    public void Vec3DTestMul3() {
        Vec3D v1 = null;
        float scalar = 1;
        Vec3D result = v1.mul(scalar);
        fail();
    }

    // Test cases for addition
    @Test
    public void Vec3DTestAdd1() {
        Vec3D v1 = new Vec3D(1, 3, 2);
        Vec3D v2 = new Vec3D(2, 1, 3);
        Vec3D result = v1.add(v2);
        assertEquals(3, result.x, DELTA);
        assertEquals(4, result.y, DELTA);
        assertEquals(5, result.z, DELTA);
    }

    @Test
    public void Vec3DTestAdd2() {
        Vec3D v1 = new Vec3D(1, Float.MAX_VALUE, 1);
        Vec3D v2 = new Vec3D(0, Float.MAX_VALUE, 0);
        Vec3D result = v1.add(v2);
        assertEquals(1, result.x, DELTA);
        assertEquals(Float.POSITIVE_INFINITY, result.y, DELTA);
        assertEquals(1, result.z, DELTA);
    }

    @Test
    public void Vec3DTestAdd3() {
        Vec3D v1 = new Vec3D(0, 0, 0);
        Vec3D v2 = new Vec3D(0, 0, 0);
        Vec3D result = v1.add(v2);
        assertEquals(0, result.x, DELTA);
        assertEquals(0, result.y, DELTA);
        assertEquals(0, result.z, DELTA);
    }

    // Test cases for substraction
    @Test
    public void Vec3DTestSub1() {
        Vec3D v1 = new Vec3D(1, 2, 3);
        Vec3D v2 = new Vec3D(2, 5, 2);
        Vec3D result = v1.sub(v2);
        assertEquals(-1, result.x, DELTA);
        assertEquals(-3, result.y, DELTA);
        assertEquals(1, result.z, DELTA);
    }

    @Test
    public void Vec3DTestSub2() {
        Vec3D v1 = new Vec3D(0, 0, 0);
        Vec3D v2 = new Vec3D(0, 0, 0);
        Vec3D result = v1.sub(v2);
        assertEquals(0, result.x, DELTA);
        assertEquals(0, result.y, DELTA);
        assertEquals(0, result.z, DELTA);
    }

    @Test
    public void Vec3DTestSub3() {
        Vec3D v1 = new Vec3D(1, Float.MAX_VALUE, 3);
        Vec3D v2 = new Vec3D(2, Float.MAX_VALUE, 2);
        Vec3D result = v1.sub(v2);
        assertEquals(-1, result.x, DELTA);
        assertEquals(0, result.y, DELTA);
        assertEquals(1, result.z, DELTA);
    }

    // Test cases for multiplication with vector 
    @Test
    public void Vec3DTestElementwiseMul1() {
        Vec3D v1 = new Vec3D(1, 2, 3);
        Vec3D v2 = new Vec3D(2, 5, 2);
        float result = v1.mul(v2);
        assertEquals(18f, result, DELTA);
    }

    @Test
    public void Vec3DTestElementwiseMul2() {
        Vec3D v1 = new Vec3D(Float.MAX_VALUE, 2, 3);
        Vec3D v2 = new Vec3D(2, 5, 2);
        float result = v1.mul(v2);
        assertEquals(Float.POSITIVE_INFINITY, result, DELTA);
    }

    @Test (expected=NullPointerException.class)
    public void Vec3DTestElementwiseMul3() {
        Vec3D v1 = new Vec3D(1, 2, 3);
        Vec3D v2 = null;
        float result = v1.mul(v2);
        fail();
    }
}
