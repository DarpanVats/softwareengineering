package exercise5;

import org.junit.Test;

import exercise5.Plane;
import exercise5.Sphere;
import exercise5.Vec3D;

import static org.junit.Assert.*;

public class EntityTest {

    @Test
    public void testPlaneEqualsSphere () {
        Plane p = new Plane(new Vec3D(0, 0, 0), new Vec3D(1, 0, 1));
        Sphere s = new Sphere(new Vec3D(2, 0, 0), 1);
        assertEquals(false, s.equals(p));
    }

}
