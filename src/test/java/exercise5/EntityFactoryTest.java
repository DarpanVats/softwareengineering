package exercise5;

import org.junit.Test;
import static org.junit.Assert.*;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

public class EntityFactoryTest {

    @Test
    public void EntityFactoryTest1() throws FileNotFoundException, IOException {
        ArrayList<Entity> entities = new ArrayList<Entity>();
        Image image = new Image(320, 240);

        EntityFactory factory = EntityFactory.get();

        entities.add(factory.create("Plane { ( 0 -1 0 ), ( 0 1 0 ) }"));
        entities.add(factory.create("Sphere { ( 5 0 10 ), 5.0 } "));
        entities.add(factory.create("Cone { ( -5 3 10 ), ( 0.5 -2 0 ), 0.3, 4.0 } "));

        image.trace(entities);
        image.write("trace2.ppm");
    }

}
