package exercise5;

import org.junit.Test;

import exercise5.Cone;
import exercise5.Entity;
import exercise5.Image;
import exercise5.Plane;
import exercise5.Sphere;
import exercise5.Vec3D;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

public class RaytracerTest {

    @Test
    public void testRaytracer() throws FileNotFoundException, IOException {
        ArrayList<Entity> entities = new ArrayList<Entity>();
        Image image = new Image(320, 240);

        entities.add(new Plane(new Vec3D(0, -1, 0), new Vec3D(0, 1, 0)));
        entities.add(new Sphere(new Vec3D(5, 0, 10), 5.0f));
        entities.add(new Cone(new Vec3D(-5, 3, 10), new Vec3D(0.5f, -2, 0), 0.3f, 4.0f));

        image.trace(entities);
        image.write("trace1.ppm");
    }

}
