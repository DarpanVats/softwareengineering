# Software Engineering SS 2019 – Exercise 5
# Software Engineering SS 2019 – Exercise 5

This is the project based on the Course Software Engineering. Implemented code works on the concept of Ray Casting for modeling 3D objects. Current version works with Gradle Script.
•	Ray tracing is a method to generate artificial images from mathematical descriptions of objects in the scene. 
Steps:
1.	Download Gradle from website : https://gradle.org/install/

2.	In Gradle command line type
a.	gradlew build
b.	gradlew Assemble  

3.	The code will write the image file in ppm format as “Trace”